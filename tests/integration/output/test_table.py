def test_list_dict(hub):
    """
    Test the standard use of display
    """
    ret = hub.output.table.display([{"foo": "bar"}])
    assert str(ret) == "+-----+\n| foo |\n+-----+\n| bar |\n+-----+"


def test_string(hub):
    # TODO put some valid html in there
    ret = hub.output.table.display(" ")
    assert str(ret) == ""


def test_dict(hub):
    ret = hub.output.table.display({"foo": [{"bar": "baz"}]})
    assert str(ret) == "+-----+\n| bar |\n+-----+\n| baz |\n+-----+"


def test_none(hub):
    ret = hub.output.table.display(None)
    assert str(ret) == ""
