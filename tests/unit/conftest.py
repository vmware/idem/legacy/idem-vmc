import aiohttp
from unittest import mock
import pytest
from dict_tools import data


@pytest.fixture
def hub(hub):
    hub.pop.sub.add(dyne_name="idem")
    return hub


@pytest.fixture
def ctx(session_backend_lib):
    """
    Override pytest-pop's ctx fixture with one that has a mocked session
    """
    yield data.NamespaceDict(
        acct=data.NamespaceDict(
            token="mock_token",
            connection=mock.create_autospec(aiohttp.ClientSession),
            api_url="mock_api_url",
            api_path="mock_api_path",
            csp_url="mock_csp_url",
            csp_path="mock_csp_path",
            org_id="mock_org_id",
        )
    )
