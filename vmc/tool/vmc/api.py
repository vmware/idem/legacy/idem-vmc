def __func_alias__(hub):
    aliases = {}
    for method in ("delete", "get", "head", "post", "patch"):
        aliases[method] = _get_caller(hub, method=method)

    return aliases


def _get_caller(hub, method):
    async def _request(ctx, ref: str, **kwargs):
        func = getattr(hub.tool.vmc.authed, method.lower())
        return await func(ctx, base_path="vmc/api", ref=ref, **kwargs,)

    return _request
