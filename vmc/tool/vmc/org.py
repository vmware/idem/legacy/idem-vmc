def __func_alias__(hub):
    aliases = {}
    for method in ("delete", "get", "head", "post", "patch"):
        aliases[method] = _get_caller(hub, method=method)

    return aliases


def _get_caller(hub, method):
    async def _request(ctx, ref: str, org_id: str = None, **kwargs):
        org_id = org_id or ctx.acct.default_org_id
        func = getattr(hub.tool.vmc.api, method.lower())
        return await func(ctx, ref=f"orgs/{org_id}/{ref}", **kwargs,)

    return _request
