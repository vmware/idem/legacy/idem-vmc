def _get_caller(hub, method):
    async def _request(
        ctx, ref: str = "", org_id: str = None, sddc_id: str = None, **kwargs
    ):
        org_id = org_id or ctx.acct.default_org_id
        sddc_id = sddc_id or ctx.acct.default_sddc_id
        func = getattr(hub.tool.vmc.org, method.lower())
        return await func(ctx, org_id=org_id, ref=f"sddcs/{sddc_id}/{ref}", **kwargs,)

    return _request


def __func_alias__(hub):
    aliases = {}
    for method in ("delete", "get", "head", "post", "patch"):
        aliases[method] = _get_caller(hub, method=method)

    return aliases
