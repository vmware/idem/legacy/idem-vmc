PUBLIC_VMC_URL = "https://vmc.vmware.com"


def __func_alias__(hub):
    aliases = {}
    for method in ("delete", "get", "head", "post", "patch"):
        aliases[method] = _get_caller(hub, method=method)

    return aliases


def _get_caller(hub, method):
    async def _request(ctx, ref: str, **kwargs):
        return await hub.tool.csp.session.request(
            method=method,
            url=ctx.acct.get("vmc_url", PUBLIC_VMC_URL),
            ref=ref,
            headers={
                "Content-Type": "application/json",
                "csp-auth-token": ctx.acct.access_token,
            },
            **kwargs,
        )

    return _request
