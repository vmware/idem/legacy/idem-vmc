"""
Connect to vmc via the vmware vsphere sdk
https://github.com/vmware/vsphere-automation-sdk-python
"""
try:
    import functools
    import inspect
    import pprint
    import requests
    from concurrent.futures.thread import ThreadPoolExecutor
    from com.vmware.vapi.std.errors_client import InvalidRequest
    from com.vmware.vmc.model_client import ErrorResponse
    from vmware.vapi.vmc import client as vmc_client

    HAS_LIBS = (True,)
except ImportError as e:
    HAS_LIBS = False, str(e)


def __virtual__(hub):
    return HAS_LIBS


def __init__(hub):
    hub.tool.vmc.client.EXECUTOR = ThreadPoolExecutor()
    hub.tool.vmc.client.VMC_CLIENT = {}


def get(hub, ctx):
    # Create a key for this ctx based on all the unique information about this ctx
    key = hash(
        ctx.acct.token.refresh_token
        + ctx.acct.vmc_url
        + ctx.acct.csp_url
        + ctx.acct.default_org_id
    )
    if key not in hub.tool.vmc.client.VMC_CLIENT:
        hub.tool.vmc.client.VMC_CLIENT[key] = vmc_client.VmcClient(
            session=requests.Session(),
            refresh_token=ctx.acct.token.refresh_token,
            vmc_url=ctx.acct.vmc_url,
            csp_url=ctx.acct.csp_url,
        )

    return hub.tool.vmc.client.VMC_CLIENT[key]


async def run(hub, ctx, ref: str, **kwargs):
    # Deconstruct the client to get the function ref
    func = hub.tool.vmc.client.get(ctx)
    for r in ref.split("."):
        func = getattr(func, r)

    # If there is an org in the signature of the function then add the org id
    sig = inspect.signature(func)
    if "org" not in kwargs and "org" in sig.parameters:
        kwargs["org"] = ctx.acct.default_org_id

    # Asynchronously call the function
    partial = functools.partial(func, **kwargs)
    try:
        return await hub.pop.Loop.run_in_executor(hub.tool.vmc.client.EXECUTOR, partial)
    except InvalidRequest as e:
        resp: ErrorResponse = e.data.convert_to(ErrorResponse)
        error_response = pprint.pformat(resp.to_dict(), width=120)
        hub.log.error(error_response)
        raise ConnectionError(error_response)
