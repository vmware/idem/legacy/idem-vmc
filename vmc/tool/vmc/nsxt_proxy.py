# TODO This will be it's own project
PATH = "policy/api/{api_version}/infra"


def __init__(hub):
    hub.tool.vmc.nsxt_proxy.CACHE = {}


def __func_alias__(hub):
    aliases = {}
    for method in ("delete", "get", "head", "post", "patch"):
        aliases[method] = _get_caller(hub, method=method)

    return aliases


def _get_caller(hub, method):
    async def _request(
        ctx,
        ref: str,
        org_id: str = None,
        sddc_id: str = None,
        manager: bool = True,
        **kwargs,
    ):
        org_id = org_id or ctx.acct.default_org_id
        sddc_id = sddc_id or ctx.acct.default_sddc_id
        key = hash(org_id + sddc_id)
        if key not in hub.tool.vmc.nsxt_proxy.CACHE:
            ret = await hub.tool.vmc.sddc.get(ctx, org_id=org_id, sddc_id=sddc_id)
            new_proxy_url: str = ret["resource_config"]["nsx_api_public_endpoint_url"]
            hub.tool.vmc.nsxt_proxy.CACHE[key] = new_proxy_url

        proxy_url = hub.tool.vmc.nsxt_proxy.CACHE[key]

        if not manager:
            proxy_url = proxy_url.rstrip("sks-nsxt-manager")

        return await hub.tool.csp.session.request(
            method=method,
            url=proxy_url,
            base_path="",
            ref=ref,
            headers={
                "Content-Type": "application/json",
                "csp-auth-token": ctx.acct.access_token,
            },
            **kwargs,
        )

    return _request
