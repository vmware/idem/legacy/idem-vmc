# These are options that are exposed on the CLI
CLI_CONFIG = {
    # Connection options
    "acct_key": {"source": "acct", "subcommands": ["_global_"]},
    "acct_file": {"source": "acct", "subcommands": ["_global_"]},
    "acct_profile": {"source": "idem", "subcommands": ["_global_"]},
    # Config reading options
    "config": {"options": ["-c"], "subcommands": ["_global_"]},
    "output": {"subcommands": ["client", "show"]},
    # SDDC import-export options
    "org_id": {"options": ["-sid"], "subcommands": ["import", "export"]},
    "folder": {"options": ["-ef"], "subcommands": ["export"]},
    "clean": {"action": "store_true", "subcommands": ["export"]},
    "gather": {"subcommands": ["export"], "nargs": "+", "positional": True,},
    "sddc_id": {
        "options": ["-ss", "-ds"],
        "subcommands": ["import", "export", "show", "client"],
    },
    # PyVMC options
    "intent": {"subcommands": ["client", "show"]},
    "id": {"options": ["-I"], "subcommands": ["client", "show"]},
}
# These are options that can show up in a config file
CONFIG = {
    "config": {"default": "", "help": "Load extra options from a configuration file"},
    # SDDC import-export options
    "folder": {"default": None, "help": "Export folder location"},
    "clean": {"default": False, "help": "Remove all previous export data"},
    "gather": {"default": None, "help": "A list of vmc resources to gather",},
    "sddc_id": {"default": None, "help": "The sddc_old to import/export from/to"},
    "org_id": {
        "default": None,
        "help": "org id to use for import/export operation."
        "Defaults to the org_id defined with acct and falls back to the default org for the given refresh_token",
    },
    # PyVMC options
    "intent": {
        "positional": True,
        "default": "",
        "help": "The name of the resource to interact with",
    },
    "id": {"default": None, "help": "The id to pass to the intent"},
    "output": {
        "default": None,
        "help": "The rend outputter to use, default is to automatically determine",
    },
}

SUBCOMMANDS = {
    "import": {x: "" for x in ("help", "desc")},
    "export": {x: "" for x in ("help", "desc")},
    "show": {x: "Display resources with PyVMC" for x in ("help", "desc")},
    "client": {x: "Run commands with PyVMC" for x in ("help", "desc")},
}
DYNE = {
    "exec": ["exec"],
    "output": ["output"],
    "states": ["states"],
    "tool": ["tool"],
    "vmc": ["vmc"],
}
