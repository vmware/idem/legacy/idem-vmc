__func_alias__ = {"list_": "list"}


async def list_(hub, ctx):
    return await hub.tool.vmc.org.get(ctx, "tbrs/support-window")


async def move(hub, ctx, sddc_id: str, target_window_id: str):
    return await hub.tool.vmc.org.put(
        ctx, f"tbrs/support-window/{target_window_id}", data=dict(sddc_id=sddc_id)
    )
