# https://vmc.vmware.com/vmc/api/orgs/{org}/account-link/sddc-connections

__func_alias__ = {"list_": "list"}


async def list_(hub, ctx, **kwargs):
    return await hub.tool.vmc.org.get(ctx, "account-link/sddc-connections", **kwargs)
