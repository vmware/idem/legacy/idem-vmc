# https://developer.vmware.com/docs/vmc/latest/sddcs/provision-spec/


async def get(hub, ctx, **kwargs):
    return await hub.tool.vmc.org.get(ctx, f"sddcs/provision-spec", **kwargs)
