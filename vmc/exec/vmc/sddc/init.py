from typing import List, Any, Dict

__func_alias__ = {"list_": "list", "id_": "id"}


def __init__(hub):
    # Map functions from init directly onto the parent sub
    for func in hub.exec.vmc.sddc.init:
        setattr(hub.exec.vmc.sddc, func.__name__, func)


async def list_(hub, ctx, include_deleted: bool = False):
    return await hub.tool.vmc.org.get(ctx, "sddcs", includeDeleted=include_deleted)


async def get(hub, ctx, sddc_id: str = None):
    return await hub.tool.vmc.sddc.get(ctx, "", sddc_id=sddc_id)


async def id_(hub, ctx, name: str, include_deleted: str = False) -> str:
    sddcs = await hub.exec.vmc.sddc.list(ctx, include_deleted=include_deleted)
    for sddc in sddcs:
        if sddc["name"] == name:
            return sddc["id"]
    raise LookupError(f"Could not find sddc named '{name}'")


def _get_subnet_id(vpc_map: Dict[str, Dict[str, Any]], region: str) -> str:
    for v in vpc_map.values():
        for subnet in v["subnets"]:
            # Compare the AWS SDDC Region with the compatible subnet region
            if (
                subnet["region_name"].lower() == region.replace("_", "-").lower()
                and subnet["compatible"]
            ):
                return subnet["subnet_id"]
    else:
        raise ValueError(f"No available subnet for region {region}")


async def create(
    hub,
    ctx,
    name: str,
    num_hosts: int,
    region: str,
    deployment_type: str,
    provider: str = None,
    delay_account_link: bool = False,
    account_link_sddc_config: List = None,
    **kwargs,
):
    provider = (provider or ctx.acct.vmc_provider).upper()
    if account_link_sddc_config is None:
        # Get connected accounts if any
        account_ids = await hub.exec.vmc.account_link.connected_account.list(ctx)

        # Get the first connected account
        if account_ids:
            account_id = account_ids[0].id

            subnets = await hub.exec.vmc.account_link.compatible_subnet.list(
                ctx, account_id=account_id, region=region
            )
            vpc_map = subnets.vpc_map

            customer_subnet_id = _get_subnet_id(vpc_map, region)

            account_link_sddc_config = [
                dict(
                    customer_subnet_ids=[customer_subnet_id],
                    connected_account_id=account_id,
                )
            ]

    sddc_config = dict(
        region=region,
        name=name,
        account_link_config={"delay_account_link": delay_account_link},
        account_link_sddc_config=account_link_sddc_config,
        provider=provider,
        num_hosts=num_hosts,
        deployment_type=deployment_type,
        **kwargs,
    )
    task = await hub.tool.vmc.org.post(ctx, "sddcs", json=sddc_config)

    return await hub.tool.vmc.task.wait(ctx, task["id"])


async def delete(
    hub,
    ctx,
    name: str,
    force: bool = False,
    retain_configuration: bool = False,
    template_name: str = None,
):
    try:
        sddc_id = await hub.exec.vmc.sddc.id(ctx, name, include_deleted=False)
    except LookupError as e:
        hub.log.info(str(e))
        return True

    task = await hub.tool.vmc.org.delete(
        ctx,
        f"sddcs/{sddc_id}",
        force=force,
        retain_configuration=retain_configuration,
        template_name=template_name,
    )
    return await hub.tool.vmc.task.wait(ctx, task["id"])


async def convert(
    hub, ctx, sddc_id: str,
):
    return await hub.tool.vmc.org.post(ctx, f"sddcs/{sddc_id}/convert")


async def update(hub, ctx, sddc_id: str, name: str):
    return await hub.tool.vmc.org.patch(ctx, f"sddcs/{sddc_id}", name=name)
