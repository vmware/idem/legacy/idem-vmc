# https://developer.vmware.com/docs/vmc/latest/sddcs/connectivity-tests/


async def get(hub, ctx, sddc_name: str, **kwargs):
    return await hub.tool.vmc.org.post(
        ctx, f"sddcs/{sddc_name}/networking/connectivity-tests", **kwargs
    )


async def meta_data(hub, ctx, sddc_name: str, **kwargs):
    return await hub.tool.vmc.org.get(
        ctx, f"sddcs/{sddc_name}/networking/connectivity-tests", **kwargs
    )
