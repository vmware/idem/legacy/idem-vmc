__func_alias__ = {"map_": "map"}


async def map_(hub, ctx, account_id: str, **kwargs):
    return await hub.tool.vmc.org.post(
        ctx, "account-link/map-customer-zones", data=kwargs
    )
