# https://vmc.vmware.com/vmc/api/orgs/{org}/account-link


async def get(hub, ctx):
    """
    Get URI To Start Linking Process
    """
    return await hub.tool.vmc.org.get(ctx, "account-link")
