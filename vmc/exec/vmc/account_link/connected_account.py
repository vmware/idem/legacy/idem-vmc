# https://vmc.vmware.com/vmc/api/orgs/{org}/account-link/connected-accounts/{linkedAccountPathId}
__func_alias__ = {"list_": "list"}


async def list_(hub, ctx, provider: str = "AWS"):
    return await hub.tool.vmc.org.get(
        ctx, "account-link/connected-accounts", provider=provider
    )


async def delete(hub, ctx, linked_account_path_id: str, force: bool = False):
    return await hub.tool.vmc.org.delete(
        ctx,
        f"account-link/connected-accounts/{linked_account_path_id}",
        forceEvenWhenSddcPresent=force,
    )
