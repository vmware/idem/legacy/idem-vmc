__func_alias__ = {"list_": "list"}


async def list_(hub, ctx, **kwargs):
    return await hub.tool.vmc.org.get(ctx, ref="tasks", **kwargs)


async def get(hub, ctx, task_id: str):
    return await hub.tool.vmc.org.get(ctx, ref=f"tasks/{task_id}")


async def delete(hub, ctx, task_id: str):
    return await hub.tool.vmc.org.post(ctx, ref=f"tasks/{task_id}", action="cancel")
