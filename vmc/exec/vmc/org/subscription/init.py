__func_alias__ = {"list_": "list"}


def __init__(hub):
    # Map functions from init directly onto the parent sub
    for func in hub.exec.vmc.org.subscription.init:
        setattr(hub.exec.vmc.org.subscription, func.__name__, func)


async def list_(hub, ctx, **kwargs):
    return await hub.tool.vmc.org.get(ctx, "subscriptions", **kwargs)


async def get(hub, ctx, subscription_id: str, **kwargs):
    return await hub.tool.vmc.org.get(ctx, f"subscriptions/{subscription_id}", **kwargs)


async def create(
    hub,
    ctx,
    commitment_term: str,
    offer_name: str,
    offer_version: str,
    product_type: str,
    quantity: int,
    region: str,
    **kwargs,
):

    return await hub.tool.vmc.org.get(
        ctx,
        f"subscriptions",
        data=dict(
            commitment_term=commitment_term,
            offer_name=offer_name,
            offer_version=offer_version,
            product_type=product_type,
            quantity=quantity,
            region=region,
            **kwargs,
        ),
    )
