__func_alias__ = {"list_": "list"}


def __init__(hub):
    # Map functions from init directly onto the parent sub
    for func in hub.exec.vmc.org.init:
        setattr(hub.exec.vmc.org, func.__name__, func)


async def list_(hub, ctx):
    return await hub.tool.vmc.api.get(ctx, ref="orgs")


async def get(hub, ctx, org_id: str = None):
    org_id = org_id or ctx.acct.default_org_id
    return await hub.tool.vmc.api.get(ctx, ref=f"orgs/{org_id}")
