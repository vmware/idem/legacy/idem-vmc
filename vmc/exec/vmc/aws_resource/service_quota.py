__func_alias__ = {"list_": "list"}


async def list_(hub, ctx):
    return await hub.tool.vmc.org.get(ctx, "aws/resources/servicequotas/requests")


async def get(hub, ctx, request_id: str):
    return await hub.tool.vmc.org.get(
        ctx, f"aws/resources/servicequotas/requests/{request_id}"
    )
